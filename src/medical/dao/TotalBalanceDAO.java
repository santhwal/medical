/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.dao;

import medical.common.FillBalanceDetail;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import medical.model.BalanceDetail;

/**
 *
 * @author ravi.kushwah
 */
public class TotalBalanceDAO {

    public int saveOrUpdate(BalanceDetail balance) {

        String query = "";
        HashMap<String, Object> existBalanceData = getBalanceData(balance.getFileNo(), balance.getRecordNo());

        if (existBalanceData.get("balanceDetail") == null) {
            query = "Insert into BalanceDetail (FileNo, RecordNo, TotalAmount, Discount, NetAmount,"
                    + "AmountInWords, Remarks) values(?,?,?,?,?,?,?)";
        } else {
            query = "Update BalanceDetail set FileNo = ?, RecordNo = ?, TotalAmount = ?, Discount = ?, "
                    + "NetAmount = ?, AmountInWords = ?, Remarks = ? where FileNo = '" + balance.getFileNo() + "' "
                    + "and RecordNo = '" + balance.getRecordNo() + "'";
        }

        int recordIndex = 0;
        try {
            PreparedStatement preparedStmt = DBConnection.getConnection().prepareStatement(query);
            preparedStmt.setString(1, balance.getFileNo());
            preparedStmt.setString(2, balance.getRecordNo());
            preparedStmt.setString(3, balance.getTotalAmount());
            preparedStmt.setString(4, balance.getDiscount());
            preparedStmt.setString(5, balance.getNetAmount());
            preparedStmt.setString(6, balance.getAmountInWords());
            preparedStmt.setString(7, balance.getRemarks());

            recordIndex = preparedStmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TotalBalanceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return recordIndex;
    }

    public HashMap<String, Object> getBalanceData(String fileNo, String recordNo) {
        FillBalanceDetail balanceDetail = new FillBalanceDetail();
        HashMap<String, Object> resultData = new HashMap<>();

        String query = "Select * from BalanceDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recordNo + "'";
        try {
            Statement statement = DBConnection.getConnection().createStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                resultData.put("balanceDetail", balanceDetail.fillDataInBalanceDetail(result));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultData;
    }

    public void delete(String fileNo, String recordNo) {
        String query = "delete from BalanceDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recordNo + "'";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            statement.execute(query);

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
