/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.dao;

import java.sql.PreparedStatement;
import medical.common.FillProductionDetail;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import medical.model.ProductDetail;

/**
 *
 * @author ravi.kushwah
 */
public class ProductSaleDAO {

    public void saveOrUpdate(String fileNo, String recordNo, List<ProductDetail> productList) throws SQLException {

        HashMap<String, List<ProductDetail>> existProductData = getProductData(fileNo, recordNo);

        if (existProductData.get("productDetail").isEmpty()) {
            productList.forEach(product
                    -> {
                try {
                    String insertQuery = "Insert into ProductDetail (FileNo, RecordNo, ProductNo, ProductName, Rate,"
                            + "Quantity) values(?,?,?,?,?,?)";
                    save(insertQuery, product);
                } catch (SQLException ex) {
                    Logger.getLogger(ProductSaleDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        } else {

            List<ProductDetail> existProductList = existProductData.get("productDetail");
            for (ProductDetail oldProduct : existProductList) {
                String updateQuery = "Update ProductDetail set FileNo = ?, RecordNo = ?, ProductNo = ?, ProductName = ?, "
                        + "Rate = ?, Quantity = ? where FileNo = '" + fileNo + "' "
                        + "and RecordNo = '" + recordNo + "' and ID = '" + oldProduct.getId() + "'";
                save(updateQuery, productList.get(existProductList.indexOf(oldProduct)));

            }
        }
    }

    public HashMap<String, List<ProductDetail>> getProductData(String fileNo, String recordNo) {
        FillProductionDetail productionDetail = new FillProductionDetail();

        HashMap<String, List<ProductDetail>> resultData = new HashMap<>();
        ArrayList<ProductDetail> productData = new ArrayList<>();

        String query = "Select * from ProductDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recordNo + "'";
        try {
            Statement statement = DBConnection.getConnection().createStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                productData.add(productionDetail.fillDataInProductDetail(result));
            }

            resultData.put("productDetail", productData);

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultData;
    }

    public void delete(String fileNo, String recordNo) {
        String query = "delete from ProductDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recordNo + "'";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            statement.execute(query);

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void save(String query, ProductDetail product) throws SQLException {
        try {
            PreparedStatement preparedStmt = DBConnection.getConnection().prepareStatement(query);
            preparedStmt.setString(1, product.getFileNo());
            preparedStmt.setString(2, product.getRecordNo());
            preparedStmt.setString(3, product.getProductNo());
            preparedStmt.setString(4, product.getProductName());
            preparedStmt.setString(5, product.getRate());
            preparedStmt.setString(6, product.getQuantity());
            preparedStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
