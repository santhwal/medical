/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ravi.kushwah
 */
public final class DBConnection {

    static String databaseFileLocation = "MedicalDatabase.accdb";
    
    
    static String driverUrl = "jdbc:ucanaccess://" + databaseFileLocation;
    private static Connection dbConnection = null;

    private DBConnection() {
        // Singleton class
    }

    public static Connection getConnection() throws SQLException {
        if (dbConnection == null) {
            File ff = new File(databaseFileLocation);
            ff.getAbsolutePath();
            driverUrl = "jdbc:ucanaccess://" + databaseFileLocation;
            dbConnection = DriverManager.getConnection(driverUrl);
        }
        return dbConnection;
    }
}
