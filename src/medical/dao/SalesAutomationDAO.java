/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.dao;

import medical.common.FillSalesDetail;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import medical.model.SaleDetail;

/**
 *
 * @author ravi.kushwah
 */
public class SalesAutomationDAO {

    public int saveOrUpdate(SaleDetail saleDetail) {
        String query = "";
        HashMap<String, Object> existSaleData = getSalesData(saleDetail.getFileNo(), saleDetail.getRecordNo());

        if (existSaleData.get("saleDetail") == null) {
            query = "Insert into SaleDetail (FileNo, RecordNo, ReferenceNo, ConsumerNo, "
                    + "CustomerName, CustomerPhoneNo, Email, AgentName, SalesDate, SalesTime, "
                    + "EntryDate, CourierName, DispatchedDate, DispatchedBy, InvoiceNo, CreditCardNo, "
                    + "CreditCardType, Address, City, State, Zip) "
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        } else {
            query = "Update SaleDetail set FileNo = ?, RecordNo = ?, ReferenceNo = ?, ConsumerNo = ?, "
                    + "CustomerName = ?, CustomerPhoneNo = ?, Email = ?, AgentName = ?, SalesDate = ?, SalesTime = ?, "
                    + "EntryDate = ?, CourierName = ?, DispatchedDate = ?, DispatchedBy = ?, InvoiceNo = ?, CreditCardNo = ?, "
                    + "CreditCardType = ?, Address = ?, City = ?, State = ?, Zip = ? where FileNo = '" + saleDetail.getFileNo()
                    + "' and RecordNo = '" + saleDetail.getRecordNo()+"'";
        }

        int recordIndex = 0;
        try {
            PreparedStatement preparedStmt = DBConnection.getConnection().prepareStatement(query);
            preparedStmt.setString(1, saleDetail.getFileNo());
            preparedStmt.setString(2, saleDetail.getRecordNo());
            preparedStmt.setString(3, saleDetail.getReferenceNo());
            preparedStmt.setString(4, saleDetail.getConsumerNo());
            preparedStmt.setString(5, saleDetail.getCustomerName());
            preparedStmt.setString(6, saleDetail.getCustomerPhoneNo());
            preparedStmt.setString(7, saleDetail.getEmail());
            preparedStmt.setString(8, saleDetail.getAgentName());
            preparedStmt.setString(9, saleDetail.getSalesDate());
            preparedStmt.setString(10, saleDetail.getSalesTime());
            preparedStmt.setString(11, saleDetail.getEntryDate());
            preparedStmt.setString(12, saleDetail.getCourierName());
            preparedStmt.setString(13, saleDetail.getDispatchedDate());
            preparedStmt.setString(14, saleDetail.getDispatchedBy());
            preparedStmt.setString(15, saleDetail.getInvoiceNo());
            preparedStmt.setString(16, saleDetail.getCreditCardNo());
            preparedStmt.setString(17, saleDetail.getCreditCardType());
            preparedStmt.setString(18, saleDetail.getAddress());
            preparedStmt.setString(19, saleDetail.getCity());
            preparedStmt.setString(20, saleDetail.getState());
            preparedStmt.setString(21, saleDetail.getZip());

            recordIndex = preparedStmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return recordIndex;
    }

    public HashMap<String, Object> getSalesData(String fileNo, String recordNo) {
        FillSalesDetail salesDetail = new FillSalesDetail();
        HashMap<String, Object> resultData = new HashMap<>();

        String query = "Select * from SaleDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recordNo + "'";
        try {
            Statement statement = DBConnection.getConnection().createStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                resultData.put("saleDetail", salesDetail.fillDataInSaleDetail(result));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultData;
    }

    public HashMap<String, Object> getSalesDataByFileNo(String fileNo) {
        FillSalesDetail salesDetail = new FillSalesDetail();
        HashMap<String, Object> resultData = new HashMap<>();

        String query = "Select TOP 1 * from SaleDetail s where s.FileNo = '" + fileNo + "' order by s.RecordNo";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                resultData.put("saleDetail", salesDetail.fillDataInSaleDetail(result));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultData;
    }

    public void delete(String fileNo, String recordNo) {

        String query = "delete from SaleDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recordNo + "'";

        try {
            Statement statement = DBConnection.getConnection().createStatement();
            statement.execute(query);

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
