/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.dao;

import medical.common.FillSalesDetail;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import medical.model.SaleDetail;

/**
 *
 * @author ravi.kushwah
 */
public class SearchDAO {

    FillSalesDetail salesDetail = new FillSalesDetail();

    public HashMap<String, Object> getSearchDataBy(String fileNo, String recNo) {
        HashMap<String, Object> resultData = new HashMap<>();

        String query = "Select * from SaleDetail s where s.FileNo = '" + fileNo + "' and "
                + "s.RecordNo = '" + recNo + "'";
        try {
            Statement statement = DBConnection.getConnection().createStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                resultData.put("saleDetail", salesDetail.fillDataInSaleDetail(result));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultData;
    }

    public HashMap<String, Object> getSearchAllData() {

        HashMap<String, Object> resultData = new HashMap<>();
        List<SaleDetail> saleDetails = new ArrayList<>();

        String query = "Select * from SaleDetail";
        try {
            Statement statement = DBConnection.getConnection().createStatement();
            ResultSet result = statement.executeQuery(query);

            while (result.next()) {
                saleDetails.add(salesDetail.fillDataInSaleDetail(result));
            }

            resultData.put("saleDetail", saleDetails);

        } catch (SQLException ex) {
            Logger.getLogger(SearchDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultData;
    }
}
