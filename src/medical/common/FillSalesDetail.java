/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import medical.model.SaleDetail;

/**
 *
 * @author sachin.anthwal
 */
public class FillSalesDetail {
    
    public SaleDetail fillDataInSaleDetail(ResultSet result) throws SQLException {
        SaleDetail saleDetail = new SaleDetail();

        saleDetail.setAddress(result.getString("Address"));
        saleDetail.setAgentName(result.getString("AgentName"));
        saleDetail.setCity(result.getString("City"));
        saleDetail.setConsumerNo(result.getString("ConsumerNo"));
        saleDetail.setCourierName(result.getString("CourierName"));
        saleDetail.setCreditCardNo(result.getString("CreditCardNo"));
        saleDetail.setCreditCardType(result.getString("CreditCardType"));
        saleDetail.setCustomerName(result.getString("CustomerName"));
        saleDetail.setCustomerPhoneNo(result.getString("CustomerPhoneNo"));
        saleDetail.setDispatchedBy(result.getString("DispatchedBy"));
        saleDetail.setDispatchedDate(result.getString("DispatchedDate"));
        saleDetail.setEmail(result.getString("Email"));
        saleDetail.setEntryDate(result.getString("EntryDate"));
        saleDetail.setFileNo(result.getString("FileNo"));
        saleDetail.setInvoiceNo(result.getString("InvoiceNo"));
        saleDetail.setRecordNo(result.getString("RecordNo"));
        saleDetail.setReferenceNo(result.getString("ReferenceNo"));
        saleDetail.setSalesDate(result.getString("SalesDate"));
        saleDetail.setSalesTime(result.getString("SalesTime"));
        saleDetail.setState(result.getString("State"));
        saleDetail.setZip(result.getString("Zip"));

        return saleDetail;
    }
}
