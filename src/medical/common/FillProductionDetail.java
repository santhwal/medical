/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import medical.model.ProductDetail;

/**
 *
 * @author sachin.anthwal
 */
public class FillProductionDetail {

    public ProductDetail fillDataInProductDetail(ResultSet result) throws SQLException {
        ProductDetail productDetail = new ProductDetail();
        
        productDetail.setId(result.getLong("ID"));
        productDetail.setFileNo(result.getString("FileNo"));
        productDetail.setRecordNo(result.getString("RecordNo"));
        productDetail.setProductNo(result.getString("ProductNo"));
        productDetail.setProductName(result.getString("ProductName"));
        productDetail.setRate(result.getString("Rate"));
        productDetail.setQuantity(result.getString("Quantity"));

        return productDetail;
    }
}
