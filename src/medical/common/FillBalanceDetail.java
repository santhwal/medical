/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medical.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import medical.model.BalanceDetail;

/**
 *
 * @author sachin.anthwal
 */
public class FillBalanceDetail {

    public BalanceDetail fillDataInBalanceDetail(ResultSet result) throws SQLException {
        BalanceDetail balanceDetail = new BalanceDetail();
        
        balanceDetail.setFileNo(result.getString("FileNo"));
        balanceDetail.setRecordNo(result.getString("RecordNo"));
        balanceDetail.setAmountInWords(result.getString("AmountInWords"));
        balanceDetail.setDiscount(result.getString("Discount"));
        balanceDetail.setNetAmount(result.getString("NetAmount"));
        balanceDetail.setRemarks(result.getString("Remarks"));
        balanceDetail.setTotalAmount(result.getString("TotalAmount"));

        return balanceDetail;
    }
}
